const express = require('express')
const { MongoKerberosError } = require('mongodb')
const app = express()
const port = 6969

// allows our app to read JSON data
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// allows our app to read data from forms
app.listen(port, () => {
  console.log(`Server is live at port 🎉💪 ${port} 💯🔥`)
})

// Step 1: Install Mongoose: czar$ npm install mongoose
// Step 2: Import Mongoose
//         Mongoose is a package that allows creation of Schemas to model our data structures

const mongoose = require('mongoose')

// Step 3: Go to MongoDB Atlas and change the Network access to 0.0.0.0
// Step 4: Get connection string and change the password to your password
// mongodb+srv://admin:<password>@zuitt-bootcamp.wvvcs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
// mongodb+srv://admin:admin1234@zuitt-bootcamp.wvvcs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

// Step 5: Change myFirstDataBase to s30.
//         MongoDB will automatically change the database for us
// mongodb+srv://admin:admin1234@zuitt-bootcamp.wvvcs.mongodb.net/s30?retryWrites=true&w=majority

// Step 6: Connecting to MongoDB Atlas --add connect() method.
// mongoose.connect

// Step 7a: Add the connection string as the first argument
// Step 7b: Add the this cobject to allow connection
/*
{
  useNewUrlParser: true,
  useUnifiedTopogy: true
}
*/

mongoose.connect(
  'mongodb+srv://admin:admin1234@zuitt-bootcamp.wvvcs.mongodb.net/s30?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

// Step 8: Set notification for connection success or failure by using .connection property of mongoose
// mongoose.connection

// Step 9: Store it in a variable called db
let db = mongoose.connection

// Step 10: console.error.bind(console) allows us to print errors in the browser console and in terminal
db.on(`error`, console.error.bind(console, 'connection error 🤡🤡🤡'))

// Step 11: If the connection is successful, output this in the console
db.once(`open`, () =>
  console.log(`We're connected to the ☁💿cloud database☁💿`)
)

// Step 12: Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
const taskSchema = new mongoose.Schema({
  // Define the fields with the corresponding data type
  // For a task, it needs a 'task name' and 'task status'
  // There's a field called 'name' and it's data type is a 'String'
  name: String,
  // There's a field called 'status' that is a 'String' and the default value is a 'pending'
  status: {
    type: String,
    default: 'PENDING 👀'
  }
})

// Models use Schemas and they act as the middleman from the server (JS code) to our database
// Step 13: Create a model
// * Models must be in a singular form and capitalized *
// * The first parameter of the Mongoose model method indicates the collection in where
//   to store the data (tasks, plural form of the model) *
const Task = mongoose.model('Task', taskSchema)

/*
Business Logic
  1) Add a functionality to check if there are duplicate tasks
    > if the task already exists in the database, we return an error
    > if the task doesn't exist in the database, we add it in the database
*/

// Step 14: Create route to add task
// app.post('/tasks', (req, res) => {
// })

// Step 15: Check if the task already exist
//          check the Task model to interact with the tasks collection
app.post('/tasks', (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name) {
      //Return a message to the client/Postman
      res.send('Duplicate task found!')
    } else {
      let newTask = new Task({
        name: req.body.name
      })
      newTask.save((saveErr, savedTask) => {
        return saveErr
          ? console.error(saveErr)
          : res.status(201).send('New task created.')
      })
    }
  })
})
// Step 16: Get all tasks
//        * Get all the contents of the task collection via the Task model
app.get('/tasks', (req, res) => {
  Task.find({}, (error, result) => {
    return error ? console.log(error) : res.status(200).json({ data: result })
  })
})

// Guided Activity
// 1) Create a User schema
/*
  username: String,
  password: String
*/
const userSchema = new mongoose.Schema({
  username: String,
  password: String
})

// 2) Make a model
const User = mongoose.model('User', userSchema)

// 3) Register a User
/*
  1) Add a functionality to check if there are duplicate users
    - If the user already exists in the database, we return an error
    - If the user data will be coming from the request's body 
    - Create a new User object with a 'username' and 'password' fields/properties
*/

// 3a) a Route for creating a user /signUp
app.post('/signUp', (req, res) => {
  User.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.username == req.body.username) {
      res.send('Duplicate user found! 🤡🤮🤡')
    } else {
      let newUser = new User({
        username: req.body.username,
        password: req.body.password
      })
      newUser.save((saveErr, savedUser) => {
        return saveErr
          ? console.error(saveErr)
          : res.status(201).send('New user created 💯💪😎')
      })
    }
  })
})
